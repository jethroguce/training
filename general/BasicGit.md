# Basic Git Tutorial

Git is a free and open source distributed [version control system] designed to handle everything from small to very large projects with speed and efficiency.

## Version Control Sytem

What is “version control”, and why should you care? Version control is a system that records changes to a file or set of files over time so that you can recall specific versions later. 

Before version control systems we commonly track versions of a file using different filename. i.e.

```
file.v1.txt
file.2019.05.14.txt
file.final.txt
file.final.final.txt
```

Admit it, you have done it before, that is very troublesome, ugly, and is prone to issues especially with large projects that has multiple persons working on it at the same time.

With __git__ we don't have to do that anymore, we can now take advantage of git's features to make beautiful version history not just of a single file but of the whole project.

## Git - Getting started

There are a lot of dirrefent ways to use Git. There are the original command-line tools, and there are many graphical user interfaces of varying capabilities. For this tutorial and most tutorials on this repository we will be using the command-line.

### Installing Git.

Before we can start using Git, we have to make it available on your computer. 
You can either install it using a package manager or via another installer.

Installers and/or instructions are available at https://git-scm.com/download 

### Creating a Git Repository

There are two ways of starting a project with git.
    1. Start from scratch `git init`
    2. Start from existing project with git `git clone`, we'll discuss this on another topic.


#### 1. Starting from scratch, Initializing a Repository in an Existing Directory
If you have a project directory that is not yet under version control and you want to start tracking it with Git, first you need to go to that directory. 

1. To start from scratch. Open a command line and go to your project directory, you should have a directory
    ```bash
    $   cd my_project
    ```


2. Initialize git
    ```bash
     my_project$   git init
    ```

This creates a new subdirectory named `.git` that contains all necessary repository files - a Git repository skeleton. At this point nothing is tracked yet. 

### 2. Tracking Files - Checking the Status of your files

Remember that each file in our working directory can be in one of two states: _tracked_ or _untracked_.

__tracked__ files are files that are in the last snapshot;
    - they can be unmodified, modified, or staged.
    - in short, they are files that Git know about.

__untracked__ files are everything else. any files that are not in the last snapshot and are not in staging area.

Since we already have a working git repository. We can always check the status of our repository using the `git status` command. 
`git status` is the main tool we use to determine which files are in which state.

```bash
my_project$ git status
On branch master

No commits yet

nothing to commit (create/copy files and use "git add" to track)
```
This means that you have a clean working directory; none of your tracked files are modified.

### 3. Tracking Files - Creating changes

Let's start creating changes to our project and track them with git.
```bash
my_project$ echo 'My Project' > README
my_project$ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)

    README

nothing added to commit but untracked files present (use "git add" to track)
```

You can see that the _README_ file is under _untracked files_. Git won't automatically track changes to files unless you explicitly tell it to do so. It is by design so you don't accidentaly begin including files that you did not mean to include.

__To start tracking files__
In order to tell _git_ to track our files, you use the command `git add`. To track _README_ file you use this command.
```bash
my_project$ git add README
```

If you run your status command again, you can see that your README file is now tracked and staged to be committed:
```bash
my_project$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    new file:   README
```
You can tell that it’s staged because it’s under the “Changes to be committed” heading. If you commit at this point, the version of the file at the time you ran git add is what will be in the subsequent historical snapshot.

### 4. Saving your changes
After you have completed your changes, nothing yet is saved to the history. Remember that staging is not yet saved, it is where we collect all files that we are going to save. Other files that are not staged won't be included in the _save_. In order to save the staged changes we use `git commit`.

```bash
my_project$ git commit
```

Running `git commit` will open the default text editor, with the following text:
```

# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
#
# On branch master
#
# Initial commit
#
# Changes to be committed:
#       new file:   README
#
~
~
~
"~/my_project/.git/COMMIT_EDITMSG" 11L, 228C
```

You can see that the default commit message contains the latest output of the git status command commented out and one empty line on top.

On this text editor we provide the __short summary of changes__ or __descriptive message__ that reflects what we changed, it is called the `commit message`.

If you are not familiar with `vi`,
    - press `i` key
    - then start typing your message "add README"
    - to save and quit the editor, press `ESC` key
    - then type `:wq`
    - then hit `return` / `enter` key

```bash
my_project$ git commit
[master (root-commit) 433e0c7] Create new file README
 1 file changed, 1 insertion(+)
 create mode 100644 README
```

Now you've created your first commit! You can see that the commit has given you some output about itself. Which branch you commited to (_master_), what commit id it has (_433e0c7_), how many files were changed, and statistics about lines added and removed in the commit.

---

That's it, we have learned
1. `git init`  - initialize
1. `git status` - status check
1. `git add` - staging 
1. `git commit` - saving

remember those commands, `status`, `add`, `commit` as they are the most used commands in git, you will use them more than any other command.

---

Exercise:
1. Edit `README`, change the contents of the file and save it.
2. Add a new file `File1.txt`, and save it
3. Edit `README` and add a new file `File2.txt`, and save it.
