# Training

this repository contains all learnings, lessons, and gatchas from beginner to advanced. I will try to collect in here everything that I know and I think would be beneficial to other people who is interested in learning a topic or two. Like any other technical document / blog, this collection of documents is not intended to be read from first to last but you can start at any point, and go to whatever topic you would want. But I would suggest to pick / learn the necessary skills before going to advanced, though sometimes it would benefit you to imagine the big picture if you move advance and then learn whatever topic you are not familiar with.

If you want to clarify or correct something please do so by contacting me or creating an issue. This document contains my learning and I would greatly appreciate to also learn from you and correct the wrong things that I might have assumed or learned somewhere.

Hope you find this document helpful. 😊

## List of Topics

### Basic
1. [Basic Git](/general/BasicGit.md)
1. Basic Programming with Python
1. Web Development with Flask
1. creating API with flask restful
1. Databases
1. Deploying to Cloud
1. Web Development: Front End framework AngularJS
1. Task Runners: Gulp
1. Creating a web application with AngularJS + Flask
1. Adding your Database
1. Continuous Integration / Continuous Deployment
1. Testing
1. Containers
1. Character & Attitude
1. Keep Learning
1. Working with Teams, Code Review
1. Your tools, IDE, plugins, command line

### Git
1. [Basic Git](/general/BasicGit.md)